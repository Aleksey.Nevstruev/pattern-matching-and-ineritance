using System.Collections.Generic;
using NUnit.Framework;
using PatternMatching;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        private static IEnumerable<TestCaseData> CaseData
        {
            get
            {
                yield return new TestCaseData(new Bus {Capacity = 90, Riders = 15}, 7.00m);
                yield return new TestCaseData(new Bus { Capacity = 90, Riders = 75 }, 5.00m);
                yield return new TestCaseData(new Bus { Capacity = 90, Riders = 85 }, 4.00m);
            }
        }

        [TestCaseSource(nameof(CaseData))]
        public void CalculateToll_ValidData_ReturnDecimal(TollCalculator calculator, decimal expected)
        {
            var actual = calculator.CalculateToll();

            Assert.That(expected, Is.EqualTo(actual));
        }
    }
}