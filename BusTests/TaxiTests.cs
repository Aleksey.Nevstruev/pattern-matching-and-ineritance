﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using PatternMatching;

namespace Tests
{
    [TestFixture]
    public class TaxiTests
    {
        private static IEnumerable<TestCaseData> CaseData
        {
            get
            {
                yield return new TestCaseData(new Taxi(), 4.50m);
                yield return new TestCaseData(new Taxi { Fares = 1 }, 3.50m);
                yield return new TestCaseData(new Taxi { Fares = 2 }, 3.00m);
                yield return new TestCaseData(new Taxi { Fares = 5 }, 2.50m);
            }
        }

        [TestCaseSource(nameof(CaseData))]
        public void CalculateToll_ValidData_ReturnDecimal(TollCalculator calculator, decimal expected)
        {
            var actual = calculator.CalculateToll();

            Assert.That(expected, Is.EqualTo(actual));
        }
    }
}
