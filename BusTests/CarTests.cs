﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using PatternMatching;

namespace Tests
{
    [TestFixture]
    public class CarTests
    {
        private static IEnumerable<TestCaseData> CaseData
        {
            get
            {
                yield return new TestCaseData(new Car(), 2.50m);
                yield return new TestCaseData(new Car { Passengers = 1 }, 2.00m);
                yield return new TestCaseData(new Car { Passengers = 2 }, 1.50m);
                yield return new TestCaseData(new Car { Passengers = 5 }, 1.00m);
            }
        }

        [TestCaseSource(nameof(CaseData))]
        public void CalculateToll_ValidData_ReturnDecimal(TollCalculator calculator, decimal expected)
        {
            var actual = calculator.CalculateToll();

            Assert.That(expected, Is.EqualTo(actual));
        }
    }
}
