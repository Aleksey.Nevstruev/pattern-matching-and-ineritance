﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using PatternMatching;

namespace Tests
{
    [TestFixture]
    public class TollCalculatorTests
    {
        private static IEnumerable<TestCaseData> CaseData
        {
            get
            {
                yield return new TestCaseData(new DateTime(2019, 3, 4, 8, 0, 0), true, 2.00m);
                yield return new TestCaseData(new DateTime(2019, 3, 6, 11, 30, 0), false, 1.50m);
                yield return new TestCaseData(new DateTime(2019, 3, 7, 17, 15, 0), true, 1.0m);
            }
        }

        [TestCaseSource(nameof(CaseData))]
        public void CalculateToll_ValidData_ReturnDecimal(DateTime time, bool inbound, decimal expected)
        {
            var actual = TollCalculator.CalculateTimeToll(time, inbound);

            Assert.That(expected, Is.EqualTo(actual));
        }
    }
}
