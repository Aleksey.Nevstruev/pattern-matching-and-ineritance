﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using PatternMatching;

namespace Tests
{
    [TestFixture]
    public class DeliveryTests
    {
        private static IEnumerable<TestCaseData> CaseData
        {
            get
            {
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 7500 }, 15.00m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 4000 }, 10.00m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 2500 }, 8.00m);
            }
        }

        [TestCaseSource(nameof(CaseData))]
        public void CalculateToll_ValidData_ReturnDecimal(TollCalculator calculator, decimal expected)
        {
            var actual = calculator.CalculateToll();

            Assert.That(expected, Is.EqualTo(actual));
        }
    }
}
