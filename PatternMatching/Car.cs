﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    public class Car : TollCalculator
    {
        private const decimal Tax = 2.00m;

        public int Passengers { get; set; }

        public override decimal CalculateToll() =>
            this.Passengers switch
            {
                0 => Tax + 0.5m,
                1 => Tax,
                2 => Tax - 0.5m,
                _ => Tax - 1.0m
            };

    }
}
