﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    public class DeliveryTruck : TollCalculator
    {
        private const decimal Tax = 10.00m;

        public int GrossWeightClass { get; set; }

        public override decimal CalculateToll() =>
            this.GrossWeightClass switch
            {
                > 5000 => Tax + 5.00m,
                < 3000 => Tax - 2.00m,
                _ => Tax,
            };
    }
}