﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace PatternMatching
{
    public class Taxi : TollCalculator
    {
        private const decimal Tax = 3.50m;

        public int Fares { get; set; }

        public override decimal CalculateToll() =>
            this.Fares switch
            {
                0 => Tax + 1.00m,
                1 => Tax,
                2 => Tax - 0.50m,
                _ => Tax - 1.00m,
            };
    }
}
