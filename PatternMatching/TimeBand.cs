﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    public enum TimeBand
    {
        MorningRush,
        Daytime,
        EveningRush,
        Overnight
    };
}
