﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    public class Bus : TollCalculator
    {
        private const decimal Tax = 5.00m;

        public int Capacity { get; set; }

        public int Riders { get; set; }

        public override decimal CalculateToll()
        {
            var tool = (double)this.Riders / (double)this.Capacity;
            return tool switch
            {
                < 0.50 => Tax + 2.00m,
                > 0.90 => Tax - 1.00m,
                _ => 5.00m,
            };
        }
    }
}
